# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/).


## [Unreleased]

### Added

- Warning message in the edition page when the petition is not published yet

### Changed

- Modified `petition/edit` to improve both clarity of page and UX :
	- moved the signatures target panel on top of "Content" section
	- added some space between input panels for friendlier UI
	- resized some input panels


## [2024-11-13]
https://framagit.org/framasoft/framapetitions/pytitions/-/commit/7708c922bf779678d224f7547c73728eada477f1
https://framagit.org/framasoft/framapetitions/pytitions/-/commit/30310d474268dcde3b6f31913ff1159eea726729

### Changed

- Modified `petition/edit` to improve both clarity of page and UX :
	- moved the petition image selector to the top of the `Social Networks` panel
	- rephrased indicator texts in the `Content` panel


## [2024-10-15]
https://framagit.org/framasoft/framapetitions/pytitions/-/commit/385abcc571e6c2ecfb8fec245df33231452f6ba5

### Fixed

- Set docker-compose command to use correct venv path, fixing an install problem
- Completed French translation
